package hot.cheese.bilet;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Artur on 13.11.15.
 */
public class ListStorage <T extends Serializable> {

    protected final SharedPreferences sharedPreferences;
    protected final Gson gson;
    private final String storageKey;
    private final ListParametrizedType listType;

    private final String KEY_SUFFIX = "_list_storage";


    public ListStorage(Class<T> itemTypeParameterClass, SharedPreferences sharedPreferences, Gson gson) {
        this(itemTypeParameterClass, sharedPreferences, gson, null);
    }

    public ListStorage(Class<T> itemTypeParameterClass, SharedPreferences sharedPreferences, Gson gson, String storageKey) {
        this.sharedPreferences = sharedPreferences;
        this.gson = gson;
        this.storageKey = createKey(itemTypeParameterClass, storageKey);
        this.listType = new ListParametrizedType(itemTypeParameterClass);
    }

    public ArrayList<T> retrieve() {
        String retrievedJson = sharedPreferences.getString(storageKey, new JsonArray().toString());

        return gson.fromJson(retrievedJson, listType);
    }

    public void store(ArrayList<T> objects) {
        sharedPreferences.edit().putString(storageKey, gson.toJson(objects, listType)).commit();
    }

    private String createKey(Class className, String storageKey) {
        return (storageKey != null && !storageKey.isEmpty()) ? storageKey : (className.getName() + KEY_SUFFIX);
    }

    public void clear() {
        sharedPreferences.edit().remove(storageKey).commit();
    }

    private static class ListParametrizedType implements ParameterizedType {

        private Type type;

        private ListParametrizedType(Type type) {
            this.type = type;
        }

        @Override
        public Type[] getActualTypeArguments() {
            return new Type[] { type };
        }

        @Override
        public Type getRawType() {
            return ArrayList.class;
        }

        @Override
        public Type getOwnerType() {
            return null;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            ListParametrizedType that = (ListParametrizedType) o;

            return !(type != null ? !type.equals(that.type) : that.type != null);
        }

        @Override
        public int hashCode() {
            return type != null ? type.hashCode() : 0;
        }
    }
}
