package hot.cheese.bilet;

import android.content.Context;
import android.preference.PreferenceManager;
import android.view.View;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by Artur Sulej on 30.03.2016.
 */
public class TicketsAdapter extends ListViewAdapter<Ticket> {

    private Gson gson;
    private ListStorage<Ticket> ticketsStorage;

    public TicketsAdapter(Context context) {
        gson = new Gson();
        ticketsStorage = new ListStorage<Ticket>(Ticket.class, PreferenceManager.getDefaultSharedPreferences(context), gson);
        super.updateData(ticketsStorage.retrieve());
    }

    @Override
    protected View createView(Context context) {
        return inflateItemView(R.layout.ticket_view);
    }

    @Override
    public void updateData(ArrayList<Ticket> tickets) {
        ticketsStorage.store(tickets);
        super.updateData(tickets);
    }

}
