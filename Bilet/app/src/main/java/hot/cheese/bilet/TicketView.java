package hot.cheese.bilet;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Artur Sulej on 30.03.2016.
 */
public class TicketView extends RelativeLayout implements ListViewAdapter.LoadableView<Ticket> {

    @Bind(R.id.destinationLocation) TextView destinationLocation;
    @Bind(R.id.startLocation) TextView startLocation;
    @Bind(R.id.startDateTime) TextView startDateTime;
    @Bind(R.id.endDateTime) TextView endDateTime;

    private final static SimpleDateFormat dateFormat = new SimpleDateFormat("H:mm\ndd.MM.yy");
    private final static SimpleDateFormat hoursMinutesFormat = new SimpleDateFormat("H:mm");

    public TicketView(Context context) {
        super(context);
    }

    public TicketView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TicketView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override
    public void loadData(Ticket ticket) {
        destinationLocation.setText(ticket.getDestinationStation());
        startLocation.setText(ticket.getStartStation());
        startDateTime.setText(formatDate(ticket.getStartDate()));
        endDateTime.setText(formatDate(ticket.getEndDate()));
    }

    private String formatDate(Date date) {
        if (isDateToday(date)) {
            return hoursMinutesFormat.format(date) + "\n" + getTodayText(getContext());
        } else {
            return dateFormat.format(date);
        }
    }

    private String getTodayText(Context context) {
        return context.getResources().getString(R.string.today);
    }


    private boolean isDateToday(Date date) {
        return areDatesTheSameDay(date, new Date());
    }

    private static boolean areDatesTheSameDay(Date date1, Date date2) {
        return (date1.getDate() == date2.getDate()
                && date1.getMonth() == date2.getMonth()
                && date1.getYear() == date2.getYear());
    }
}
