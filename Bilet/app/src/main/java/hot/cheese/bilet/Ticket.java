package hot.cheese.bilet;

import android.util.Log;

import java.io.File;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Artur Sulej on 29.03.2016.
 */
public class Ticket implements Serializable {

    private final File file;
    private String startStation;
    private String destinationStation;
    private Date startDate;
    private Date endDate;
    private Transporter transporter;

    public Ticket(String ticketText, File file) throws IllegalArgumentException {
        try {
            String[] ticketParts = ticketText.split("\\n");

            for (int i = 0; i < ticketParts.length; i++) {  //fixme logging
                Log.i("ticketParts " + i, ticketParts[i]);
            }

            transporter = Transporter.nameToTransporter(ticketParts[0]);
            if (transporter == null) {
                throw new IllegalArgumentException("Invalid transporter name.");
            }

            //            startDate = createDate(ticketParts[10], ticketParts[8]);
            //            endDate = createDate(ticketParts[18], ticketParts[16]);
            createDates(ticketParts);


            startStation = ticketParts[12].trim();
            destinationStation = ticketParts[14].replace("->", "").trim();

        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Unprocessable ticket text: " + file.getAbsolutePath());
        }
        this.file = file;
        Log.i("ticketfile", file.getAbsolutePath());
    }

    private void createDates(String[] ticketParts) throws NullPointerException, ParseException {
        final CharSequence TEXT = "Zapłacono i wystawiono dnia:";
        String textWithYear = null;
        for (int i = ticketParts.length - 1; i >= 0; i--) {
            String partText = ticketParts[i];
            if (partText.contains(TEXT)) {
                textWithYear = partText;
                break;
            }
        }

        DateFormat ticketGenerationDateFormat = new SimpleDateFormat("yyy-MM-dd hh:mm:ss");
        textWithYear = textWithYear.replace(TEXT, "");

        Date ticketGenerationDate = ticketGenerationDateFormat.parse(textWithYear);


        String startTimeString = ticketParts[10].trim();
        String startDateString = ticketParts[8].trim();

        Calendar ticketGenerationCalendar = new GregorianCalendar();
        ticketGenerationCalendar.setTime(ticketGenerationDate);

        String startDateText = startDateString + " " + ticketGenerationCalendar.get(Calendar.YEAR) + " " + startTimeString;
        Log.i("startDateText", startDateText);

        DateFormat format = new SimpleDateFormat("dd.MM yyyy hh:mm");

        startDate = format.parse(startDateText);

        if (startDate.before(ticketGenerationDate)) {
            startDate.setYear(startDate.getYear() + 1);
            ticketGenerationCalendar.set(Calendar.YEAR, ticketGenerationCalendar.get(Calendar.YEAR) + 1);
        }

        String endTimeString = ticketParts[18].trim();
        String endDateString = ticketParts[16].trim();

        String endDateText = endDateString + " " + ticketGenerationCalendar.get(Calendar.YEAR) + " " + endTimeString;
        Log.i("startDateText", startDateText);

        endDate = format.parse(endDateText);
    }

    //    private Date createDate(String timeString, String dateString) throws ParseException {
    //        String startDateText = dateString + " " + new Date().getYear() + " " + timeString; //fixme temp year
    //        //rok wziąć z: data startu to najbliższa możliwa data po dacie wystawienia biletu
    //        //szukać zamiast brać po indexie - case: dodany komentarz o pracach modernizacyjnych
    //        Log.i("startDateText", startDateText);
    //
    //        DateFormat format = new SimpleDateFormat("dd.MM yyyy hh:mm");
    //        return format.parse(startDateText);
    //    }

    public enum Transporter {
        PKP_INTERCITY("\"PKP Intercity\"");

        private final String nameText;

        Transporter(String nameText) {
            this.nameText = nameText;
        }

        private static Transporter nameToTransporter(String nameText) {
            for (Transporter transporter : Transporter.values()) {
                if (transporter.nameText.equals(nameText)) {
                    return transporter;
                }
            }
            return null;
        }
    }

    public File getFile() {
        return file;
    }

    public String getStartStation() {
        return startStation;
    }

    public String getDestinationStation() {
        return destinationStation;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Transporter getTransporter() {
        return transporter;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "destinationStation='" + destinationStation + '\'' +
                ", startStation='" + startStation + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", transporter=" + transporter.nameText +
                '}';
    }
}
