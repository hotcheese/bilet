package hot.cheese.bilet;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TicketsListActivity extends Activity {

    @Bind(R.id.ticketsListView) ListView ticketsListView;
    private TicketsAdapter ticketsAdapter;
    private PDFTicketsOpener pdfTicketsOpener;
    @Bind(R.id.refresh) SwipeRefreshLayout refreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tickets_list);
        ButterKnife.bind(this);
        enableRefreshingSpinner();

        ticketsAdapter = new TicketsAdapter(this);
        ticketsListView.setAdapter(ticketsAdapter);

        initPdfTicketsOpener();
        initOnClickListener();
        initRefreshingListener();

        pdfTicketsOpener.fetchTicketsAsync();
    }

    private void enableRefreshingSpinner() {
        refreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(true);
            }
        });
    }

    private void initRefreshingListener() {
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pdfTicketsOpener.fetchTicketsAsync();
            }
        });
    }

    private void initOnClickListener() {
        ticketsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openFile(ticketsAdapter.getItem(position).getFile());
            }
        });
    }

    private void initPdfTicketsOpener() {
        pdfTicketsOpener = new PDFTicketsOpener(getApplicationContext(), new PDFTicketsOpener.FetchListener() {
            @Override
            public void onTicketsFetched(ArrayList<Ticket> tickets) {
                ticketsAdapter.updateData(tickets);
                refreshLayout.setRefreshing(false);
            }
        });
    }

    private void openFile(File file) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.fromFile(file));
        startActivity(intent);
    }

}
