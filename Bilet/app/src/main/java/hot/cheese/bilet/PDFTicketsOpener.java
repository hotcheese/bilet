package hot.cheese.bilet;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.util.PDFBoxResourceLoader;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Artur Sulej on 13.04.2016.
 */
public class PDFTicketsOpener {

    private final String TICKET_REGEX = "^bilet.*\\.pdf$";
    private Context context;
    private final FetchListener fetchListener;

    public PDFTicketsOpener(Context context, FetchListener fetchListener) {
        this.context = context;
        this.fetchListener = fetchListener;
    }

    public void fetchTicketsAsync() {
        new FetchPDFTicketsTask().execute();
    }

    private ArrayList<Ticket> fetchTickets() {
        PDFBoxResourceLoader.init(context);

        File downloadsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        ArrayList<File> ticketsFiles = new ArrayList<File>();
        File[] listFiles = downloadsDir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return filename.matches(TICKET_REGEX);
            }
        });

        if (listFiles != null) {
            for (File file : listFiles) {
                ticketsFiles.add(file);
                Log.i("pdffiles", file.getName());
            }

            if (!ticketsFiles.isEmpty()) {
                //add caching all tickets as text
                //Is it possible to make it work faster? e.g ignore fonts stuff?
                //https://github.com/TomRoush/PdfBox-Android/issues/32
                //https://github.com/TomRoush/PdfBox-Android/issues/69
                ArrayList<Ticket> tickets = new ArrayList<>();

                for (File file : ticketsFiles) {
                    PDDocument doc = null;
                    PDFTextStripper stripper;

                    try {
                        doc = PDDocument.load(file);

                        stripper = new PDFTextStripper();
                        String text = stripper.getText(doc);

                        tickets.add(new Ticket(text, file));
                    } catch (IOException | IllegalArgumentException e) {
                        e.printStackTrace();
                        Log.i("openerexc", file.getAbsolutePath());
                    } finally {
                        closeDoc(doc);
                    }
                }

                Collections.sort(tickets, new Comparator<Ticket>() {
                    @Override
                    public int compare(Ticket lhs, Ticket rhs) {
                        if (lhs.getStartDate().before(rhs.getStartDate())) {
                            return 1;
                        } else {
                            return -1;
                        }
                    }
                });
                return tickets;

            }
        }
        return null;
    }

    private void closeDoc(PDDocument doc) {
        if (doc != null) {
            try {
                doc.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private class FetchPDFTicketsTask extends AsyncTask<Void, Void, ArrayList<Ticket>> {

        @Override
        protected ArrayList<Ticket> doInBackground(Void... params) {
            return fetchTickets();
        }

        @Override
        protected void onPostExecute(ArrayList<Ticket> tickets) {
            if (tickets != null) {
                fetchListener.onTicketsFetched(tickets);
            }
        }
    }


    public interface FetchListener {

        void onTicketsFetched(ArrayList<Ticket> tickets);
    }

}
