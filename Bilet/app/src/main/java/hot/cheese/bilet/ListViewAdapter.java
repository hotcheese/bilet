package hot.cheese.bilet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Artur Sulej on 30.03.2016.
 * https://gist.github.com/hot-cheese/e0e90e7d5a0167db9d7b
 */
public abstract class ListViewAdapter <T> extends BaseAdapter {

    protected ListView listView;
    private ArrayList<T> objects = new ArrayList<T>();

    public void updateData(ArrayList<T> objects) {
        this.objects = objects;
        notifyDataSetChanged();
    }

    public void appendData(ArrayList<T> objectsPortion) {
        this.objects.addAll(objectsPortion);
        notifyDataSetChanged();
    }

    public void appendData(T object) {
        this.objects.add(object);
        notifyDataSetChanged();
    }

    public ArrayList<T> getData() {
        return objects;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public T getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (listView == null) {
            listView = (ListView) parent;
        }
        return provideView(convertView, objects.get(position));
    }

    protected abstract View createView(Context context);

    protected void loadData(LoadableView<T> loadableView, T object) {
        loadableView.loadData(object);
    }

    private View provideView(View convertView, T object) {
        if (convertView == null) {
            convertView = createView(listView.getContext());
        }

        @SuppressWarnings("unchecked")
        LoadableView<T> loadableView = ((LoadableView<T>) convertView);
        loadData(loadableView, object);
        return (View) loadableView;
    }

    protected View inflateItemView(int layoutId){
        LayoutInflater inflater = LayoutInflater.from(listView.getContext());
        return inflater.inflate(layoutId, listView, false);
    }

    public interface LoadableView <T> {
        void loadData(T object);
    }
}
